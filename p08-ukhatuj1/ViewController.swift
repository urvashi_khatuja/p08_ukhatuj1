//
//  ViewController.swift
//  p08-ukhatuj1
//
//  Created by urvashi khatuja on 5/9/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
         answer_lbl.isHidden = true;
        
        if let scene = MainMenuScene(fileNamed : "MainMenu"){
        
        scene.scaleMode = .aspectFill
            
        }
        
        
        
    } 
   //1. array of questions 
    
    var questions = [
    "What’s the name of this city in the United States with a bell in the near middle of the city?",
    "What is the name of the spaceship first landed on the moon?",
    "How many people can fit in jumbo jet?",
    "Put a pillow on your fridge day is officially celebrated on?",
    "Cherophobia is also known as?",
    "Why do some actor/actrsses never sweat?",
    "What is the antonym of DOMINOS ?",
    "I have ocean but no water. Who am I?",
    "Another word for lexican?",
    "Professor of mobile app and game developement"
    ];
    var answers = [ "Allentown, Pennsylvania",
                    "Apollo 11",
                    "660",
                    "29th May",
                    "Fear of fun",
                    "Because they have huge fans !!",
                    "Domi Doesn't No :p",
                    "A World Map !!",
                    "Dictionary",
                    "Prof. Patrick Madden !!"]
    
    var currentQuestionIndex = 0
    var currentAnswerIndex = 0

    @IBOutlet weak var answer_lbl: UILabel!
    
    @IBAction func didTapAnswer(_ sender: Any) {
        print("answer tapper")
        
        answer_lbl.isHidden = false;
        
        
        currentAnswerIndex += 1;
        let numberOfAnswer = answers.count;
        let nextAnswerIndex = currentAnswerIndex % numberOfAnswer;
        
        self.answer_lbl.text = answers[nextAnswerIndex]
    }
   
    @IBOutlet weak var question_label: UILabel!
    
    @IBAction func nextQuestionDidTap(_ sender: Any)
    {
        currentQuestionIndex += 1;
        let numberOfQuestions = questions.count;
        let nextQuestionIndex = currentQuestionIndex % numberOfQuestions;
    
        question_label.text = questions[nextQuestionIndex];
        
    }
    
    
   
    
    
  //  @IBOutlet weak var answer_label: UIButton!

   // @IBAction func nextAnswerTap(_ sender: Any) {
        
     //   currentAnswerIndex += 1;
       // let numberOfAnswer = answers.count;
        //let nextAnswerIndex = currentAnswerIndex % numberOfAnswer;
        
      //  answer_label.text = answers [nextAnswerIndex];
        
    //}
}

