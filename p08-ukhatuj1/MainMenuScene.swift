//
//  MainMenuScene.swift
//  p08-ukhatuj1
//
//  Created by urvashi khatuja on 5/10/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

import UIKit
import SpriteKit

class MainMenuScene: SKScene {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        for touch in touches {
        
            let location = touch.location(in: self);
            
            if atPoint(location).name == "Start"{
            
                if let scene = MainMenuScene(fileNamed : "ViewController"){
                    
                    scene.scaleMode = .aspectFill
                    
                }
 
                
            }
            
        
        }
    }
} // class
